import React from "react";

const AboutUs: React.FC = () => {
  return (
    <section className={`flex flex-col bg-white py-20 text-3xl md:text-4xl`}>
      <div className="container mx-auto px-11">
        <p>
          <strong>We will help you ship better apps, faster.</strong> Our team
          of expert engineers has created the best user experience sin some of
          the most popular apps worldwide.
        </p>
      </div>
    </section>
  );
};

export default AboutUs;
